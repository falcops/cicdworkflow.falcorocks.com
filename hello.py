import os
from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello_world():
    commit = os.environ.get("CI_COMMIT_SHORT_SHA")
    return f"<p>Hello, World! {commit}</p>"
